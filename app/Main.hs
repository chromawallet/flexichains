module Main where

import qualified Data.Map as Map
import Simple
import Helper

mytx = SimpleTx [("1", 0)] ["000"] [SimpleRecord ("2", 0) False 1 "000"]
myblock = SimpleBlock [mytx]

main = print (apply_block_map myblock (1::Int)
              (Map.fromList [(("1", 0), SimpleRecord ("1", 0) False 1 "000")]))
