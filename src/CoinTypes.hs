{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-} 

module CoinTypes (Coin(..),
                 ColorValue(..),
                 AdditiveCoin(..),
                 AdditiveColorValue(..),
                 TransactionBuilder(..)) where

import CoreTypes

class (Record r k, Eq pkt) => Coin r k cv pkt | r -> k, r -> cv, r -> pkt where
  coin_value :: r -> cv
  coin_spent :: r -> Bool
  coin_pubkey :: r -> pkt
  

class (Coin r k cv pkt, Num cv, Ord cv) => AdditiveCoin r k cv pkt

class ColorValue cv cid | cv -> cid where
  get_color :: cv -> cid
  same_color :: cv -> cv -> Bool
  
class (ColorValue cv cid, Num cv, Ord cv) => AdditiveColorValue cv cid | cv -> cid

class (Coin rt kt cv pkt, Transaction tx rt kt) => TransactionBuilder tx rt kt cv pkt dt st | tx -> rt, rt -> cv, rt -> pkt, rt -> kt, tx -> dt, tx -> st where
  make_transaction :: [rt] -> [(cv, pkt)] -> (dt -> rt -> Maybe st) -> Either String  tx
  
