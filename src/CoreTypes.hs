{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-} 

module CoreTypes (
 Record, Transaction, Block,
 get_key, tx_dependencies, apply_tx, validate_block_header,
 block_transactions
) where

class Ord k => Record r k | r -> k where
  get_key :: r -> k

class Record rt kt => Transaction tx rt kt | tx -> rt, rt -> kt where
  tx_dependencies :: Record rt kt => tx -> [kt]
  apply_tx :: Record rt kt => tx -> (kt -> Maybe rt) -> Either String [rt]
  
class Block b cs tx | b -> tx, b -> cs where
  validate_block_header :: b -> cs -> Either String cs
  block_transactions :: b -> [tx]

class BlockChainState bcs bt rt cs | bcs -> bt, bcs -> rt, bt -> cs where
  init_blockchain_state :: cs -> [rt] -> bcs
  apply_block :: bcs -> bt -> Either String (bcs, [rt])