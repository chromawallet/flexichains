{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-} 

module Simple ( SimpleRecord(..), SimpleTx(..), SimpleBlock(..) ) where

import CoreTypes
import CoinTypes

type CoinId = (String, Int)
type Pubkey = String
type Digest = String
data SimpleRecord = SimpleRecord { coinid :: CoinId, spent :: Bool, 
                                   value :: Integer, pubkey :: Pubkey } 
                  deriving (Show)
                           
type Signature = String                           
data SimpleTx = SimpleTx { inputs :: [CoinId], 
                           signatures :: [Signature],
                           outputs :: [SimpleRecord] } 
              deriving (Show)

instance Record SimpleRecord CoinId where
  get_key = coinid
  
instance Coin SimpleRecord CoinId Integer Pubkey where
  coin_value = value
  coin_spent = spent
  coin_pubkey = pubkey

mark_spent r = r { spent = True }

check_signature (pk, sig) = pk == sig

instance Transaction SimpleTx SimpleRecord CoinId where
  tx_dependencies = inputs
  apply_tx tx res = case (mapM res (inputs tx)) of
    Just input_coins -> 
      let all_unspent = not $ any spent input_coins
          sum_inputs = sum $ map value input_coins
          sum_outputs = sum $ map value (outputs tx)
          signatures_ok = all check_signature (zip (map pubkey input_coins) 
                                               (signatures tx))
      in if (all_unspent && (sum_inputs >= sum_outputs) && signatures_ok)
         then Right ((outputs tx) ++ (map mark_spent input_coins))
         else Left "validation failed"
    Nothing -> Left "missing inputs"
    
data SimpleBlock = SimpleBlock { transactions :: [SimpleTx] } deriving (Show)

instance Block SimpleBlock Int SimpleTx where
  validate_block_header b cs = Right cs
  block_transactions = transactions
  
instance TransactionBuilder SimpleTx SimpleRecord CoinId Integer Pubkey Digest Signature where
  make_transaction inputs targets signer = 
    case mapM (signer "") inputs of
      Just signatures -> 
        let outputFromTarget (cv, pk) = SimpleRecord { coinid = ("xxx", 0), spent = False,
                                                       value = cv, pubkey = pk }
        in Right SimpleTx { inputs = map coinid inputs, signatures = signatures,
                          outputs = map outputFromTarget targets }
      Nothing -> Left "couldn't sign one of inputs"
