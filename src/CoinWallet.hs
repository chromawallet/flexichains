module CoinWallet () where

import Data.Map (Map)
import qualified Data.Map as Map
import CoreTypes
import CoinTypes

data WalletState kt ct pkt = WalletState { coins :: Map kt ct, pubkeys :: [pkt] }

updateWalletState :: Coin ct kt cv pkt => WalletState kt ct pkt -> ct -> WalletState kt ct pkt
updateWalletState ws coin = if (not $ null $ filter ((==) (coin_pubkey coin)) (pubkeys ws))
                               || (Map.member (get_key coin) (coins ws))
                            then ws { coins = (Map.insert (get_key coin) coin (coins ws)) } 
                            else ws
                                 
selectCoins :: AdditiveCoin ct k cv pk => WalletState k ct pk -> cv -> Maybe ([ct], WalletState k ct pk)
selectCoins ws cv = let unspent = filter (not . coin_spent) $ map snd $ Map.toList $ coins ws
                        coin_rs = scanl (+) 0 $ map coin_value unspent
                        coins_with_rs = zip coin_rs unspent
                        selected_coins = takeWhile (\(rs, coin) -> (rs < cv)) coins_with_rs
                    in if last coin_rs >= cv
                       then Just ((snd $ unzip selected_coins), ws)
                       else Nothing

