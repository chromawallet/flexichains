module Helper ( apply_tx_map, apply_block_map ) where

import Data.Map (Map)
import qualified Data.Map as Map
import Control.Monad (foldM)
import CoreTypes

apply_tx_map :: (Transaction tx rt kt) => tx -> Map kt rt -> Either String (Map kt rt)
apply_tx_map tx res = do
  new_records <- apply_tx tx ((flip Map.lookup) res)
  return (Map.fromList (map (\r -> (get_key r, r)) new_records))


apply_block_map :: (Block b cs tx, Transaction tx rt kt) => b -> cs -> Map kt rt -> Either String (cs, Map kt rt)
apply_block_map b cs res = do
  cs' <- (validate_block_header b cs)
  res' <- let apply_tx' res tx = do 
                new_records <- apply_tx_map tx res
                return (Map.union new_records res)
          in foldM apply_tx' res (block_transactions b)
  return (cs', res')

